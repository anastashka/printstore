import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-user.create',
  templateUrl: './user.create.component.html',
  styles: []
})
export class UserCreateComponent implements OnInit {

  public user = this.formBuilder.group({
    Email: '',
    Password: ''
  });

  @Output() public passEntry: EventEmitter<any> = new EventEmitter();

  constructor(
    public activeModal: NgbActiveModal,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    console.log(this.user);
  }

  saveUser() {
    this.passEntry.emit(this.user);
    this.activeModal.close(this.user);
  }

  onClose() {
    this.activeModal.close();
  }
}
