import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PrintStoreComponent } from './components/print.store/print.store.component';
import { PrintStoreItemComponent } from './components/print.store.item/print.store.item.component';
import { PrintStoreAllComponent } from './components/print.store.all/print.store.all.component';


const routes: Routes = [
  { path: '', component: PrintStoreComponent ,
    children: [
      {path: 'item', component: PrintStoreItemComponent},
      {path: 'all', component: PrintStoreAllComponent}
    ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class PrintStoreRoutingModule { }
