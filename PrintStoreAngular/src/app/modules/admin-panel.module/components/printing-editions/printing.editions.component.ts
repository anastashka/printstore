import { AddPrintingComponent } from './add.printing/add.printing.component';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EditPrintingComponent } from './edit.printing/edit.printing.component';
import { AdminService } from '../../services/admin.service';
import { PrintType } from 'src/app/enums/print.type';
import { PrintStatus } from 'src/app/enums/print.status';
import { SortDirection } from 'src/app/enums/sort.direction';

@Component({
  selector: 'app-printing.editions',
  templateUrl: './printing.editions.component.html',
  styles: []
})

export class PrintingEditionsComponent implements OnInit {

  public defaultItem = 0;
  public currencies;
  public printingEditions;
  public isPaginationNextDisabled = false;
  public isPaginationPreviousDisabled = false;
  public authorSearchString: string;
  public titleSearch: string;
  public descriptionSearch: string;
  public bsModalRef: BsModalRef;
  public storeItemsCount: number;
  public printType = PrintType;
  public printStatus = PrintStatus;
  public sortDirection = SortDirection.Ascending;
  public searchGroup = {
    titleSearchString: '',
    authorSearchString: '',
    descriptionSearchString: ''
  };
  public printingEditionModel = {
    TitleName: '',
    Status: '',
    Type: '',
    Description: '',
    Price: '',
    Currency: '',
    Author: ''
  };

  constructor(
    private service: AdminService,
    private router: Router,
    public modalService: NgbModal) { }

  search() {
    this.defaultItem = 0;
    if (this.searchGroup.authorSearchString) {
      localStorage.removeItem('filter');
      localStorage.removeItem('peStatus');
      localStorage.removeItem('peType');
      localStorage.setItem('filter', JSON.stringify('Author.FirstName'));

      this.service.getNextPageOfPrintingEditions(this.defaultItem, this.sortDirection, this.searchGroup.authorSearchString)
      .subscribe(res => {
        this.printingEditions = res;
      },
      err => {
        console.log(err);
      });
    }

    if (this.searchGroup.titleSearchString) {
      localStorage.removeItem('filter');
      localStorage.removeItem('peStatus');
      localStorage.removeItem('peType');
      localStorage.setItem('filter', JSON.stringify('PrintingEdition.NameEdition'));
      this.service.getNextPageOfPrintingEditions(this.defaultItem, this.sortDirection, this.searchGroup.titleSearchString)
      .subscribe(res => {
        this.printingEditions = res;
      },
      err => {
        console.log(err);
      });
    }

    if (this.searchGroup.descriptionSearchString) {
      localStorage.removeItem('filter');
      localStorage.removeItem('peStatus');
      localStorage.removeItem('peType');
      localStorage.setItem('filter', JSON.stringify('PrintingEdition.Description'));
      this.service.getNextPageOfPrintingEditions(this.defaultItem, this.sortDirection, this.searchGroup.descriptionSearchString)
      .subscribe(res => {
        this.printingEditions = res;
      },
      err => {
        console.log(err);
      });
    }
  }

  onSort() {
    if (this.sortDirection === SortDirection.None) {
      this.sortDirection = SortDirection.Ascending;
      this.ngOnInit();
      return;
    }
    if (this.sortDirection === SortDirection.Ascending) {
      this.sorting(this.sortDirection);
      this.sortDirection = SortDirection.Descending;
    }
    if (this.sortDirection === SortDirection.Descending) {
      this.sorting(this.sortDirection);
      this.sortDirection = SortDirection.None;
    }
  }

  convertToCurrency(currencyName) {
    localStorage.removeItem('currentCurrencyName');
    localStorage.setItem('currentCurrencyName', currencyName);

    this.service.convertToCurrency(currencyName).subscribe((printingEditions) => {
      this.printingEditions = printingEditions;
    });
  }

  sorting(sortDirection) {
    this.service.sortByPrice(sortDirection).subscribe(res => {
      this.printingEditions = res;
    },
    err => {
      console.log(err);
    });
  }

  getStatus(statusEd) {
    let status;
    if (statusEd === PrintStatus.Available) {
      status = 'Available';
    }
    if (statusEd === PrintStatus.NotAvailable) {
      status = 'Not available';
    }

    if (statusEd === PrintStatus.UnderTheOrder) {
      status = 'Under the order';
    }
    return status;
  }

  getType(typeEd) {
    let type;
    if (typeEd === PrintType.Magazine) {
      type = 'Magazine';
    }
    if (typeEd === PrintType.Book) {
      type = 'Book';
    }

    if (typeEd === PrintType.NewsPaper) {
      type = 'News paper';
    }
    return type;
  }

  ngOnInit() {
    this.service.storeItemsCount.subscribe(count => this.storeItemsCount = count);
    if (this.printingEditions === undefined) {
      this.service.getNextPageOfPrintingEditions(this.defaultItem, this.sortDirection, null).subscribe(
        res => {
            this.printingEditions = res;
            this.defaultItem = 4;
        },
        err => {
          console.log(err);
        },
      );
    }
    this.service.getCurrencies().subscribe(res => {
      this.currencies = res;
    });

    localStorage.removeItem('currentCurrencyName');
    localStorage.setItem('currentCurrencyName', 'USD');
  }

  paginationNext() {
    let countOfElements = 0;
    this.service.getNextPageOfPrintingEditions(this.defaultItem, this.sortDirection, null).subscribe((res: any[]) => {
      this.printingEditions = res;
      this.isPaginationPreviousDisabled = false;
      countOfElements = res.length;
      if (countOfElements < this.service.amountOfElementsOnPage) {
        this.isPaginationNextDisabled = true;
      }
    });
    this.defaultItem += this.service.amountOfElementsOnPage;
  }

  paginationPrevious() {
    this.defaultItem -= this.service.amountOfElementsOnPage;
    if (this.defaultItem <= this.service.amountOfElementsOnPage) {
      this.isPaginationPreviousDisabled = true;
    }
    if (this.defaultItem >= 0) {
      this.service.getPreviousPageOfPrintingEditions(this.defaultItem, this.sortDirection, null).subscribe((res) => {
        this.printingEditions = res;
        this.isPaginationNextDisabled = false;
      });
    }
  }

  ifNextDisabled() {
    return this.isPaginationNextDisabled;
  }

  ifPreviousDisabled() {
    return this.isPaginationPreviousDisabled;
  }

  onLogout() {
    localStorage.removeItem('refreshToken');
    this.router.navigate(['/user/login']);
  }

  filterByType(type) {
    localStorage.removeItem('filter');
    localStorage.setItem('filter', JSON.stringify('PrintingEdition.Type'));

    localStorage.removeItem('peType');
    localStorage.removeItem('peStatus');
    localStorage.setItem('peType', JSON.stringify(type));

    this.defaultItem = 0;
    this.service.getNextPageOfPrintingEditions(this.defaultItem, this.sortDirection, null).subscribe(
      res => {
          this.printingEditions = res;
      },
      err => {
        console.log(err);
      },
    );
  }

  filterByStatus(status) {
    localStorage.removeItem('filter');
    localStorage.setItem('filter', JSON.stringify('PrintingEdition.Status'));

    localStorage.removeItem('peStatus');
    localStorage.removeItem('peType');
    localStorage.setItem('peStatus', JSON.stringify(status));

    this.defaultItem = 0;
    this.service.getNextPageOfPrintingEditions(this.defaultItem, this.sortDirection, null).subscribe(
      res => {
          this.printingEditions = res;
      },
      err => {
        console.log(err);
      },
    );
  }

  delete(printingEdition) {
    const id = printingEdition.prtintingEditionId;
    this.service.deleteAuthInPrintEdition(id).subscribe(
      () => {
          this.ngOnInit();
      },
      err => {
        console.log(err);
      },
    );
  }

  openEditModal(entity) {
    const printingEdition = {
      Id: entity.prtintingEditionId,
      TitleName: entity.printingEditionTitle,
      Status: entity.printingEditionStatus,
      Type: entity.prtintingEditionType,
      Description: entity.prtintingEditionDescription,
      Price: entity.printingEditionPrice,
      CurrencyName: entity.currencyName,
      CurrencyId: entity.currencyId,
      ImageUrl: entity.printingEditionImage,
      AuthorsList: entity.authorsList
    };

    const modalRef = this.modalService.open(EditPrintingComponent);
    modalRef.componentInstance.printingEdition = printingEdition;
    modalRef.result.then((result) => {
      if (result) {
        this.ngOnInit();
        console.log(result);
      }
    });
  }

  addNewPost() {
    const modalRef = this.modalService.open(AddPrintingComponent);
    modalRef.result.then((result) => {
      if (result) {
        this.ngOnInit();
        console.log(result);
      }
    });
  }
}
