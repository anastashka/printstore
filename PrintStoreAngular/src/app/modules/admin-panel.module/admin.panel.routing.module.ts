import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminPanelComponent } from './components/admin-panel/admin-panel.component';
import { AuthGuard } from '../interceptor/auth/auth.guard';
import { GetUsersComponent } from './components/users/get.users.component';
import { AuthorsComponent } from './components/authors/authors.component';
import { PrintingEditionsComponent } from './components/printing-editions/printing.editions.component';
import { OrdersComponent } from './components/orders/orders.component';


const routes: Routes = [
  {path: '', component: AdminPanelComponent, canActivate: [AuthGuard], data : {permittedRoles: ['admin']},
  children: [
    {path: 'getusers', component: GetUsersComponent, canActivate: [AuthGuard], data: {permittedRoles: ['admin']}},
    {path: 'authors', component: AuthorsComponent, canActivate: [AuthGuard], data: {permittedRoles: ['admin']}},
    {path: 'printingEditions', component: PrintingEditionsComponent, canActivate: [AuthGuard], data : {permittedRoles: ['admin']}},
    {path: 'orders', component: OrdersComponent, canActivate: [AuthGuard], data: {permittedRoles: ['admin']}}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class AdminPanelRoutingModule { }
