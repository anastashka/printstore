import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminPanelComponent } from './components/admin-panel/admin-panel.component';
import { AuthorsComponent } from './components/authors/authors.component';
import { OrdersComponent } from './components/orders/orders.component';
import { PrintingEditionsComponent } from './components/printing-editions/printing.editions.component';
import { GetUsersComponent } from './components/users/get.users.component';
import { AdminPanelRoutingModule } from './admin.panel.routing.module';
import { CreateAuthorComponent } from './components/authors/create.author/create.author.component';
import { EditAuthorComponent } from './components/authors/edit.author/edit.author.component';
import { UserCreateComponent } from './components/users/user.create/user.create.component';
import { UserEditComponent } from './components/users/user.edit/user.edit.component';
import { AddPrintingComponent } from './components/printing-editions/add.printing/add.printing.component';
import { EditPrintingComponent } from './components/printing-editions/edit.printing/edit.printing.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    AdminPanelComponent,
    AuthorsComponent,
    OrdersComponent,
    PrintingEditionsComponent,
    GetUsersComponent
  ],
  imports: [
    CommonModule,
    AdminPanelRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})

export class AdminPanelModule { }
