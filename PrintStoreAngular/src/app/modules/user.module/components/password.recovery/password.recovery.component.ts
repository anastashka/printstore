import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-password.recovery',
  templateUrl: './password.recovery.component.html',
  styleUrls: ['./password.recovery.component.css']
})
export class PasswordRecoveryComponent implements OnInit {

  public email;

  constructor(private service: UserService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.email = params['email'];
    });
  }

  onReset() {
    this.service.forgotPassword(this.email).subscribe((res) => {
      if (res === 200) {
        this.router.navigateByUrl('/user/newpass');
      }
    });
  }
}
