import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormControl } from '@angular/forms';
import { debounceTime, tap, switchMap, finalize } from 'rxjs/operators';
import { AdminService } from '../../../services/admin.service';
import { PrintType } from 'src/app/enums/print.type';
import { PrintStatus } from 'src/app/enums/print.status';


@Component({
  selector: 'app-add.printing',
  templateUrl: './add.printing.component.html',
  styleUrls: []
})

export class AddPrintingComponent implements OnInit {
  public authorsList;
  public authors;
  public searchForm = new FormControl();
  public printtype = PrintType;
  public printEnumStatus = PrintStatus;
  public errorMsg: string;
  public isLoading: boolean;
  public printType: string[] = ['Magazine', 'Book', 'NewsPaper'];
  public printStatusString: string[] = ['Under the order', 'Available', 'Not available'];
  public currencies: any[] = [];
  public printingEdition = this.formBuilder.group({
    TitleName: '',
    Status: '',
    Type: '',
    Description: '',
    Price: '',
    Currency: '',
    ImageUrl: '',
    Author: ''
  });

  @Output() passEntry: EventEmitter<any> = new EventEmitter();

  constructor(
    public activeModal: NgbActiveModal,
    private formBuilder: FormBuilder, private service: AdminService) {
      this.service.getCurrencies().subscribe(data => {
        Object.assign(this.currencies, data);
      }, () => { console.log('Error while gettig currency data.'); });
    }

  search(event) {
    this.service.getAuthorsByName(event.query).subscribe(data => {
        this.authors = data;
    });
  }

  onPostFormSubmit() {
    const postData = {
      PrintingEditionTitle: this.printingEdition.get('TitleName').value,
      PrintingEditionStatus: this.printingEdition.get('Status').value,
      PrtintingEditionType: this.printingEdition.get('Type').value,
      PrtintingEditionDescription: this.printingEdition.get('Description').value,
      PrintingEditionPrice: this.printingEdition.get('Price').value,
      AuthorsList: this.printingEdition.get('Author').value,
      CurrencyId: this.printingEdition.get('Currency').value,
      PrintingEditionImage: this.printingEdition.get('ImageUrl').value
    };

    this.service.addPrintingEdition(postData).subscribe(() => {
        console.log();
          this.passEntry.emit('OK');
          this.activeModal.close();
      });
  }

  ngOnInit() {
    this.searchForm.valueChanges
    .pipe(
      debounceTime(500),
      tap(() => {
        this.errorMsg = '""';
        this.authorsList = [];
        this.isLoading = true;
      }),
      switchMap(value => this.service.getAuthorsByName(value)
        .pipe(
          finalize(() => {
            this.isLoading = false;
          }),
        )
      )
    ).subscribe(data => {
      if (data === undefined) {
        this.errorMsg = data['Error'];
        this.authorsList = [];
      }
      if (data !== undefined) {
        this.errorMsg = '';
        this.authorsList = data;
      }

      console.log(this.authorsList);
    });
  }

  onClose() {
    this.activeModal.close();
  }
}
