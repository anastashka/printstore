import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { UserService } from '../../user.module/services/user.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private router: Router,private service : UserService) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
      if (localStorage.getItem('refreshToken') === null) {
        this.router.navigate(['/user/login']);
        return false;
      }
      
      let roles = next.data['permittedRoles'] as Array<string>;
      
      if(roles && !this.service.roleMatch(roles)){
        this.router.navigate(['/forbidden']);
        return false;          
      } 

      return true;
  }
}
