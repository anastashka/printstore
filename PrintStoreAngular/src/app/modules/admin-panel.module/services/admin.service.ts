import { SortDirection } from 'src/app/enums/sort.direction';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { PrintType } from 'src/app/enums/print.type';
import { PrintStatus } from 'src/app/enums/print.status';

@Injectable({
    providedIn: 'root'
})

export class AdminService {

  public storeItemsCount;
  public amountOfElementsOnPage = 4;

  constructor(private http: HttpClient) {
    this.storeItemsCount = this.getCountOfEditionsInStore();
  }

  getCountOfEditionsInStore(): any {
    return this.http.get('/printStore/GetCountOfEditionsInStore');
  }

  filterByOrderStatus(ordersStatusIsPayed: any) {
    const options = { params: new HttpParams().set('value', ordersStatusIsPayed.toString())};
    return this.http.get('/admin/FilterByOrderStatus', options);
  }

  convertToCurrency(currencyName: any) {
    const options = { params: new HttpParams().set('currencyName', currencyName)};
    return this.http.get('/printStore/GetAllWithCurrencyConvert', options);
  }

  searchAuthorForAuthorTable(authorSearchString) {
    return this.http.get(`/printStore/SearchAuthorsForAuthrTable?filter=${authorSearchString}`);
  }

  sortAuthorByLastName(sortDirection) {
    return this.http.get('/printStore/SortByAuthorsLastName?value=' + sortDirection);
  }

  sortAuthorByFirstName(sortDirection) {
    return this.http.get(`/printStore/SortByAuthorsFirstName?value=${sortDirection}`);
  }

  orderSort(sortDirection) {
    let sortColumn = localStorage.getItem('peStatus');

    if (sortColumn === null) {
      sortColumn = 'none';
    }

    const options = { params: new HttpParams()
      .set('value', sortDirection)
      .set('column', sortColumn)
    };
    return this.http.get('/admin/OrderSort', options);
  }

  editPrintingEdition(postData) {
    return this.http.post('/admin/EditPrintingEdition', postData);
  }

  searchDescription(descriptionSearchString: string) {
    return this.http.get(`/printStore/FilterByPrintingEditionDescription?filter=${descriptionSearchString}`);
  }

  getAuthorsByName(query: any) {
    return this.http.get(`/printStore/GetFilteredAuthors?filter=${query}`);
  }

  deleteAuthInPrintEdition(printingEdition: any) {
    return this.http.post('/admin/DeleteEdition', printingEdition);
  }

  addPrintingEdition(postData) {
    return this.http.post('/admin/AddPrintingEdition', postData);
  }

  getCurrencies() {
    return this.http.get('/printStore/GetCurrencies');
  }

  sortByPrice(direction) {
    const currency = localStorage.getItem('currentCurrencyName');
    const options = { params: new HttpParams().set('value', direction.toString())
      .set('currentCurrencyName', currency) };

    return this.http.get('/printStore/SortByPrintingEditionPrice', options);
  }

  editUser(user: any) {
    const body = {
      Id: user.Id,
      FirstName: user.FirstName,
      LastName: user.LastName,
      Email: user.Email
    };

    return this.http.post('/users/Edit', body);
  }

  editAuthor(author) {
    const body = {
      Id: author.Id,
      FirstName: author.FirstName,
      LastName: author.LastName
    };
    return this.http.post('/admin/EditAuthor', body);
  }

  createAuthor(author) {
    const body = {
      FirstName: author.FirstName,
      LastName: author.LastName
    };

    return this.http.post('/admin/AddAuthor', body);
  }

  getAllAuthors() {
    return this.http.get('/admin/GetAllAuthors');
  }

  getNextPageOfPrintingEditions(item: any, sortOrder, filter) {
    const lastElement = item + this.amountOfElementsOnPage;
    const currency = localStorage.getItem('currentCurrencyName');
    let columnFilter = JSON.parse(localStorage.getItem('filter'));
    let peType = localStorage.getItem('peType');
    let peStatus = localStorage.getItem('peStatus');

    if (columnFilter === null) {
      columnFilter = 'none';
    }
    if (peType === null) {
        peType = PrintType.None.toString();
    }
    if (peStatus === null) {
      peStatus = PrintStatus.None.toString();
    }
    if (filter === null) {
      filter = 'none';
    }

    const options = { params: new HttpParams().set('firstElement', item.toString())
      .set('lastElement', lastElement.toString())
      .set('currentCurrencyName', currency)
      .set('order', sortOrder)
      .set('column', columnFilter)
      .set('filter', filter)
      .set('type', peType)
      .set('status', peStatus)
    };
    return this.http.get('/printStore/GetAuthorsInPrintingEditionsPage', options);
  }

  getPreviousPageOfPrintingEditions(item: number, sortOrder, filter) {
    let lastElement;
    if (item > 0) {
      lastElement = item - this.amountOfElementsOnPage;
    }
    if (item === 0) {
       lastElement = item + this.amountOfElementsOnPage;
    }
    const currency = localStorage.getItem('currentCurrencyName');
    let columnFilter = JSON.parse(localStorage.getItem('filter'));
    let peType = localStorage.getItem('peType');
    let peStatus = localStorage.getItem('peStatus');

    if (filter === null) {
      filter = 'none';
    }
    if (columnFilter === null) {
      columnFilter = 'none';
    }
    if (peType === null) {
        peType = PrintType.None.toString();
    }
    if (peStatus === null) {
      peStatus = PrintStatus.None.toString();
    }

    const options = { params: new HttpParams().set('firstElement', lastElement.toString())
      .set('lastElement', item.toString())
      .set('currentCurrencyName', currency)
      .set('order', sortOrder)
      .set('column', columnFilter)
      .set('filter', filter)
      .set('type', peType)
      .set('status', peStatus)
    };

    return this.http.get('/printStore/GetAuthorsInPrintingEditionsPage', options);
  }


  getAllUsers() {
    return this.http.get('/admin/GetAllUsersProfile');
  }

  createUser(user) {
    const body = {
      Email: user.Email,
      Password: user.Password
    };
    return this.http.post('/users/Create', body);
  }

  deleteUser(userId) {
    const body = {
      Id: userId
    };

    return this.http.post('/users/Delete', body);
  }

  getAllUsersOrders() {
    return this.http.get('/admin/GetAllUsersOrder');
  }

  deleteAuthor(authorId) {
    return this.http.post('/admin/DeleteAuthor', authorId);
  }

  GetSortedAuthorsByName(sortDirection: any, sortColumn) {
    const options = { params: new HttpParams().set('order', sortDirection.toString())
      .set('column', sortColumn) };
    return this.http.get('/printStore/GetSortedAuthorsByName', options);
  }

  getSortedUsersByLastName(sortDirection: any) {
    const options = { params: new HttpParams().set('value', sortDirection.toString())};

    return this.http.get('/admin/GetSortedUsersByLastName', options);
  }
  getSortedUsersByFirstName(sortDirection: any) {
    const options = { params: new HttpParams().set('value', sortDirection.toString())};

    return this.http.get('/admin/GetSortedUsersByFirstName', options);
  }
}
