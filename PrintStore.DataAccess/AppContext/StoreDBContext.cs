﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace PrintStore.DataAccess.Entities.Base
{
    public class StoreDBContext : IdentityDbContext<ApplicationUser>
    {
        public StoreDBContext(DbContextOptions<StoreDBContext> options)
            : base(options) {}

        public DbSet<Author> Authors { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Payment> Payments { get; set; }
        public DbSet<Currency> Currencies { get; set; }
        public DbSet<PrintingEdition> PrintingEditions { get; set; }
        public DbSet<AuthorInPrintingEditions> AuthorInPrintingEditions { get; set; }
        public DbSet<PrintingEditionsInOrders> PrintingEditionsInOrders { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<AuthorInPrintingEditions>().HasKey(key => new { key.AuthorId, key.PrintingEditionId });

            modelBuilder.Entity<AuthorInPrintingEditions>()
                .HasOne<Author>(auth => auth.Author)
                .WithMany()
                .HasForeignKey(fk => fk.AuthorId)
                .HasPrincipalKey(pk => pk.Id)
                .IsRequired();

            modelBuilder.Entity<AuthorInPrintingEditions>()
                .HasOne<PrintingEdition>(pe => pe.PrintingEdition)
                .WithMany()
                .HasForeignKey(fk => fk.PrintingEditionId)
                .HasPrincipalKey(pk => pk.Id)
                .IsRequired();

            modelBuilder.Entity<PrintingEditionsInOrders>().HasKey(key => new { key.OrderId, key.PrintingEditionId });

            modelBuilder.Entity<PrintingEditionsInOrders>()
                .HasOne<Order>(ord => ord.Order)
                .WithMany()
                .HasForeignKey(fk => fk.OrderId)
                .HasPrincipalKey(pk => pk.Id)
                .IsRequired();

            modelBuilder.Entity<PrintingEditionsInOrders>()
                .HasOne<PrintingEdition>(pe => pe.PrintingEdition)
                .WithMany()
                .HasForeignKey(fk => fk.PrintingEditionId)
                .HasPrincipalKey(pk => pk.Id)
                .IsRequired();

            modelBuilder.Entity<Order>()
                .HasOne<ApplicationUser>(user => user.ApplicationUser)
                .WithMany()
                .HasForeignKey(fk => fk.ApplicationUserId)
                .IsRequired();

            modelBuilder.Entity<Order>()
                .HasOne(paym => paym.Payment)
                .WithOne(ord => ord.Order)
                .HasForeignKey<Payment>(fk => fk.OrderId);

            modelBuilder.Entity<Payment>()
               .HasOne(ord => ord.Order)
               .WithOne(paym => paym.Payment);

            modelBuilder.Entity<PrintingEdition>()
                .HasOne<Currency>(cur => cur.Currency)
                .WithMany()
                .HasForeignKey(fk => fk.CurrencyId)
                .IsRequired();
        }
    }
}
