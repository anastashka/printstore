import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserComponent } from './components/user/user.component';
import { RegistrationComponent } from './components/registration/registration.component';
import { LoginComponent } from './components/login/login.component';
import { ConfirmEmailComponent } from './components/confirm.email/confirm.email.component';
import { EditUserComponent } from './components/edit.user.profile/edit.component';
import { AuthGuard } from '../interceptor/auth/auth.guard';
import { PasswordRecoveryComponent } from './components/password.recovery/password.recovery.component';
import { NewPasswordGenerateComponent } from './components/new.password.generate/new.password.generate.component';

const routes: Routes = [
  {path: '', component: UserComponent,
    children: [
      {path: 'registration', component: RegistrationComponent},
      {path: 'login', component: LoginComponent},
      {path: 'edit', component: EditUserComponent, canActivate: [AuthGuard]},
      {path: 'confirm', component: ConfirmEmailComponent},
      {path: 'recovery', component: PasswordRecoveryComponent},
      {path: 'newpass', component: NewPasswordGenerateComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserModuleRoutingModule { }
