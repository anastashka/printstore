import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-create.author',
  templateUrl: './create.author.component.html',
  styles: []
})

export class CreateAuthorComponent implements OnInit {

  public author = this.formBuilder.group({
    FirstName: '',
    LastName: ''
  });

  @Output() passEntry: EventEmitter<any> = new EventEmitter();

  constructor(
    public activeModal: NgbActiveModal,
    private formBuilder: FormBuilder) { }

  ngOnInit() {
    console.log(this.author);
  }

  onClose() {
    this.activeModal.close();
  }

  saveAuthor() {
    this.passEntry.emit(this.author);
    this.activeModal.close(this.author);
  }
}
