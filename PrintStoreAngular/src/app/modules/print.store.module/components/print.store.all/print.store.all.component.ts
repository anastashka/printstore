import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { LabelType, Options } from 'ng5-slider';
import { PrintStoreService } from '../../services/print.store.service';
import { PrintStatus } from 'src/app/enums/print.status';
import { SortDirection } from 'src/app/enums/sort.direction';
import { PrintType } from 'src/app/enums/print.type';

@Component({
  selector: 'app-print.store.all',
  templateUrl: './print.store.all.component.html',
  styleUrls: ['./print.store.all.component.css']
})

export class PrintStoreAllComponent implements OnInit {

  private sortDirection = SortDirection.Ascending;
  public printingEditions: any = [];
  private sortPriceFrom: number;
  private sortPriceTo: number;
  public currencies;
  public titleSearch: string;
  public descriptionSearch: string;
  public isPaginationNextDisabled = false;
  public isPaginationPreviousDisabled = false;
  public printType = PrintType;
  public printStatus = PrintStatus;
  public storeItemsCount: number;
  public defaultItem = 0;
  public productList: any = [];
  public minValue = 10;
  public maxValue = 150;
  public options: Options = {
    floor: 0,
    ceil: 200,
    translate: (value: number, label: LabelType): string => {
      switch (label) {
        case LabelType.Low:
          this.sortPriceFrom = value;
          return '$' + value;
        case LabelType.High:
            this.sortPriceTo = value;
          return '$' + value;
        default:
          return '$' + value;
      }
    }
  };

  private searchGroup = {
    titleSearchString: '',
    authorSearchString: ''
  };

  constructor( private service: PrintStoreService, private router: Router) { }

  ngOnInit() {
    localStorage.setItem('filter', JSON.stringify('Author.Id'));
    localStorage.removeItem('peStatus');
    localStorage.removeItem('peType');
    this.service.getCurrencies().subscribe(res => {
      this.currencies = res;
    });

    this.service.storeItemsCount.subscribe(count => this.storeItemsCount = count);

    this.isPaginationPreviousDisabled = true;

    this.service.printingEditions.subscribe((printingEditions) => {
      this.printingEditions = printingEditions;
      this.defaultItem += this.service.amountOfElementsOnPage;
    });
  }

  paginationNext() {
    let countOfElements = 0;
    this.service.getNextPageOfPrintingEditions(this.defaultItem, this.sortDirection, null).subscribe((res: any[]) => {
      this.printingEditions = res;
      this.isPaginationPreviousDisabled = false;
      countOfElements = res.length;
      if (countOfElements < this.service.amountOfElementsOnPage) {
        this.isPaginationNextDisabled = true;
      }
    });
    this.defaultItem += this.service.amountOfElementsOnPage;
  }

  paginationPrevious() {
    this.defaultItem -= this.service.amountOfElementsOnPage;
    if (this.defaultItem <= this.service.amountOfElementsOnPage) {
      this.isPaginationPreviousDisabled = true;
    }
    if (this.defaultItem >= 0) {
      this.service.getPreviousPageOfPrintingEditions(this.defaultItem, this.sortDirection, null).subscribe((res) => {
        this.printingEditions = res;
        this.isPaginationNextDisabled = false;
      });
    }
  }

  ifNextDisabled() {
    return this.isPaginationNextDisabled;
  }

  ifPreviousDisabled() {
    return this.isPaginationPreviousDisabled;
  }

  default() {
    localStorage.removeItem('filter');
    localStorage.removeItem('peStatus');
    localStorage.removeItem('peType');
    this.service.getCurrencies().subscribe(res => {
      this.currencies = res;
    });

    this.service.printingEditions.subscribe(printingEditions => this.printingEditions = printingEditions);

    localStorage.setItem('currentCurrencyName', 'USD');
  }

  convertToCurrency(currencyName) {
    localStorage.removeItem('currentCurrencyName');
    localStorage.setItem('currentCurrencyName', currencyName);
    this.defaultItem = 0;
    this.service.getNextPageOfPrintingEditions(this.defaultItem, this.sortDirection, null).subscribe((res) => {
      this.printingEditions = res;
      this.isPaginationPreviousDisabled = false;
    });
  }

  getDisabled(printingEditionStatus) {
    if (printingEditionStatus === PrintStatus.NotAvailable) {
      return true;
    }
    return false;
  }

  getPriceRange() {
    this.defaultItem = 0;
    this.service.sortByPriceRange(this.sortPriceFrom, this.sortPriceTo, this.defaultItem).subscribe(res => {
      this.printingEditions = res;
    },
    err => {
      console.log(err);
    });
  }

  search( ) {
    this.defaultItem = 0;
    localStorage.setItem('filter', JSON.stringify('PrintingEdition.NameEdition'));
    this.service.searchAnyProduct(this.searchGroup.titleSearchString, this.defaultItem).subscribe((res) =>
        this.printingEditions = res
        );
  }

  openProductItem(printingEdition) {
    const navigationExtras: NavigationExtras = {
      queryParams: {
        PrtintingEditionId: printingEdition.prtintingEditionId
      }
    };

    this.router.navigate(['/store/item'], navigationExtras);
  }

  getColor(statusEd) {
    if (statusEd === PrintStatus.Available) {
      return 'green';
    }
    if (statusEd === PrintStatus.NotAvailable) {
      return 'red';
    }
    if (statusEd === PrintStatus.UnderTheOrder) {
      return 'orange';
    }
  }

  getStatus(statusEd) {
    if (statusEd === PrintStatus.Available) {
      return 'Available';
    }
    if (statusEd === PrintStatus.NotAvailable) {
      return 'Not available';
    }
    if (statusEd === PrintStatus.UnderTheOrder) {
      return 'Under the order';
    }
  }

  getType(typeEd) {
    let type;
    if (typeEd === PrintType.Magazine) {
      type = 'Magazine';
    }
    if (typeEd === PrintType.Book) {
      type = 'Book';
    }
    if (typeEd === PrintType.NewsPaper) {
      type = 'News paper';
    }
    return type;
  }

  onSort() {
    if (this.sortDirection === SortDirection.None) {
      this.sortDirection = SortDirection.Ascending;
      this.ngOnInit();
      return;
    }
    if (this.sortDirection === SortDirection.Ascending) {
      this.sortingPrice(this.sortDirection);
      this.sortDirection = SortDirection.Descending;
    }
    if (this.sortDirection === SortDirection.Descending) {
      this.sortingPrice(this.sortDirection);
      this.sortDirection = SortDirection.None;
    }
  }

  sortByName() {
    if (this.sortDirection === SortDirection.None) {
      this.sortDirection = SortDirection.Ascending;
      this.ngOnInit();
      return;
    }
    if (this.sortDirection === SortDirection.Ascending) {
      this.sortingAuthors(this.sortDirection);
      this.sortDirection = SortDirection.Descending;
    }
    if (this.sortDirection === SortDirection.Descending) {
      this.sortingAuthors(this.sortDirection);
      this.sortDirection = SortDirection.None;
    }
  }

  sortingAuthors(sortDirection) {
    localStorage.removeItem('filter');
    localStorage.setItem('filter', JSON.stringify('Author.FirstName'));

    this.defaultItem = 0;

    this.service.getNextPageOfPrintingEditions(this.defaultItem, sortDirection, null).subscribe(res => {
      this.printingEditions = res;
      this.defaultItem = this.service.amountOfElementsOnPage;
    },
    err => {
      console.log(err);
    });
  }

  sortingPrice(sortDirection) {
    localStorage.removeItem('filter');
    localStorage.setItem('filter', JSON.stringify('PrintingEdition.Price'));

    localStorage.removeItem('filter');
    localStorage.setItem('filter', JSON.stringify('PrintingEdition.Price'));

    this.defaultItem = 0;

    this.service.getNextPageOfPrintingEditions(this.defaultItem, sortDirection, null).subscribe(res => {
      this.printingEditions = res;
      this.defaultItem = this.service.amountOfElementsOnPage;
    },
    err => {
      console.log(err);
    });
  }

  filterByType(type) {
    localStorage.removeItem('peType');
    localStorage.setItem('peType', JSON.stringify(type));

    this.defaultItem = 0;

    this.service.getNextPageOfPrintingEditions(this.defaultItem, this.sortDirection, null).subscribe(
      res => {
          this.printingEditions = res;
          this.defaultItem = this.service.amountOfElementsOnPage;
      },
      err => {
        console.log(err);
      },
    );
  }

  filterByStatus(status) {
    localStorage.removeItem('peStatus');
    localStorage.setItem('peStatus', JSON.stringify(status));

    this.defaultItem = 0;

    this.service.getNextPageOfPrintingEditions(this.defaultItem, this.sortDirection, null).subscribe(
      res => {
          this.printingEditions = res;
          this.defaultItem = this.service.amountOfElementsOnPage;
      },
      err => {
        console.log(err);
      },
    );
  }

  addToBasket(printingEdition) {
    this.service.addToBasket(printingEdition);
  }
}
