import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormControl } from '@angular/forms';
import { debounceTime, tap, switchMap, finalize } from 'rxjs/operators';
import { AdminService } from '../../../services/admin.service';
import { PrintType } from 'src/app/enums/print.type';
import { PrintStatus } from 'src/app/enums/print.status';

@Component({
  selector: 'app-edit.printing',
  templateUrl: './edit.printing.component.html',
  styles: []
})

export class EditPrintingComponent implements OnInit {

  public authorsList;
  public authors;
  public searchForm = new FormControl();
  public currencies;
  public errorMsg: string;
  public isLoading: boolean;
  public printtype = PrintType;
  public printEnumStatus = PrintStatus;
  public printType: string[] = ['Magazine', 'Book', 'NewsPaper'];
  public printStatusString: string[] = ['Under the order', 'Available', 'Not available'];
  private newLocal = 'Error';

  @Input() public printingEdition;
  @Output() public passEntry: EventEmitter<any> = new EventEmitter();

  constructor(
    public activeModal: NgbActiveModal,
    public service: AdminService) {

    this.service.getCurrencies().subscribe(data => {
      this.currencies = data;
    }, error => { console.log('Error while gettig currency data.'); });
    }

  search(event) {
    this.service.getAuthorsByName(event.query).subscribe(data => {
        this.authors = data;
    });
  }

  onPostFormSubmit() {
    const postData = {
      PrintingEditionTitle: this.printingEdition.TitleName,
      PrtintingEditionId: this.printingEdition.Id,
      PrintingEditionStatus: this.printingEdition.Status,
      PrtintingEditionType: this.printingEdition.Type,
      PrtintingEditionDescription: this.printingEdition.Description,
      PrintingEditionPrice: this.printingEdition.Price,
      AuthorsList: this.printingEdition.AuthorsList,
      CurrencyId: this.printingEdition.CurrencyId,
      PrintingEditionImage: this.printingEdition.ImageUrl
    };

    this.service.editPrintingEdition(postData).subscribe(() => {
      console.log();
        this.passEntry.emit('OK');
        this.activeModal.close();
    });
  }

  ngOnInit() {
    this.searchForm.valueChanges
    .pipe(
      debounceTime(500),
      tap(() => {
        this.errorMsg = '';
        this.authorsList = [];
        this.isLoading = true;
      }),
      switchMap(value => this.service.getAuthorsByName(value)
        .pipe(
          finalize(() => {
            this.isLoading = false;
          }),
        )
      )
    ).subscribe(data => {
      if (data === undefined) {
        this.errorMsg = data[this.newLocal];
        this.authorsList = [];
      }
      if (data !== undefined) {
        this.errorMsg = '';
        this.authorsList = data;
      }
    });
  }

  onClose() {
    this.activeModal.close();
  }
}
