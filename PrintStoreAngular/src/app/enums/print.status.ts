export enum PrintStatus {
    None = 0,
    UnderTheOrder = 1,
    Available = 2,
    NotAvailable = 3,

}
