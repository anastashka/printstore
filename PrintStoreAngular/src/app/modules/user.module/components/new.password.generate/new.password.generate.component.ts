import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { UserService } from '../../services/user.service';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-new.password.generate',
  templateUrl: './new.password.generate.component.html',
  styleUrls: ['./new.password.generate.component.css']

})

export class NewPasswordGenerateComponent implements OnInit {

  public email;
  public formPasswords = this.formBuilder.group({
    Id: '',
    NewPassword: ['', [ Validators.minLength(6)]],
    ConfirmNewPassword: ['', [ Validators.minLength(6)]],
    OldPassword: ''
  }, {validator: this.comparePasswords});

  constructor(private service: UserService, private route: ActivatedRoute, private formBuilder: FormBuilder, private router: Router) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.email = params['email'];
    });
  }

  onReset() {
    this.service.forgotPassword(this.email).subscribe((res) => {
      if (res === 200) {
        this.router.navigateByUrl('/user/newpass');
      }
    });
  }

  comparePasswords(formGr: FormGroup) {
    const confirmPaswCtrl = formGr.get('ConfirmNewPassword');
    if (confirmPaswCtrl.errors == null || 'passwordMismatch' in confirmPaswCtrl.errors) {
      confirmPaswCtrl.setErrors(null);
      if (formGr.get('NewPassword').value !== confirmPaswCtrl.value) {
        confirmPaswCtrl.setErrors({passwordMismatch: true});
      }
    }
  }
}
