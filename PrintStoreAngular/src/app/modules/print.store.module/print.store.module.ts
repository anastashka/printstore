import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrintStoreRoutingModule } from './print.store.routing.module';
import { PrintStoreComponent } from './components/print.store/print.store.component';
import { PrintStoreAllComponent } from './components/print.store.all/print.store.all.component';
import { PrintStoreItemComponent } from './components/print.store.item/print.store.item.component';
import { FormsModule } from '@angular/forms';
import { SliderModule } from 'primeng/slider';
import { Ng5SliderModule } from 'ng5-slider';

@NgModule({
  declarations: [PrintStoreComponent, PrintStoreAllComponent, PrintStoreItemComponent],
  imports: [
    CommonModule,
    PrintStoreRoutingModule,
    FormsModule,
    SliderModule,
    Ng5SliderModule,
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class PrintStoreModule { }
