import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Injectable({
    providedIn: 'root'
  })

export class ShoppingCartService {

  public bSubject = new BehaviorSubject([]);

  constructor(public modalService: NgbModal) { }

  setSubjectList(productList) {
    const list: any = [] ;
    let product;

    for (const i of productList.length) {
      if (typeof(productList[i]) === 'string') {
        product = JSON.parse(productList[i]);
      }
      if (typeof(productList[i]) !== 'string') {
        product = productList[i];
      }
      const sameProduct = list.find(prodId => prodId.prtintingEditionId === product.prtintingEditionId);
      if (sameProduct === undefined) {
        list.push(product);
        list.find(prodId => prodId.prtintingEditionId === product.prtintingEditionId).printinEditionQuantityForOrder = 1;
      }
      if (sameProduct !== undefined) {
        list.find(prodId => prodId.prtintingEditionId === product.prtintingEditionId).printinEditionQuantityForOrder += 1;
      }
    }

    this.bSubject.next(list);
  }
}
