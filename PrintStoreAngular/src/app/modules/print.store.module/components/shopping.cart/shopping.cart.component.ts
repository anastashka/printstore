import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UserService } from 'src/app/modules/user.module/services/user.service';
import { ShoppingCartService } from '../../services/shopping.cart.service';
import { StripeCheckoutLoader, StripeCheckoutHandler } from 'ng-stripe-checkout';
import { PrintStoreService } from '../../services/print.store.service';

@Component({
  selector: 'app-shopping.cart',
  templateUrl: './shopping.cart.component.html',
  styleUrls: ['./shopping.cart.component.css']
})

export class ShoppingCartComponent implements OnInit {

  public paymentNumber: any;
  public list: any[] = [];
  public userDetails;
  public orderAmount;
  public orderId;
  public tokenId;
  public stripe = (<any> window).Stripe('pk_test_FMGLP7vKlKsjogGBkdxams5S00dXmbLOXT'); // use your test publishable key
  public elements = this.stripe.elements();
  private stripeCheckoutHandler: StripeCheckoutHandler;

  constructor(
    public activeModal: NgbActiveModal,
    public modalService: NgbModal,
    public service: PrintStoreService,
    public userService: UserService,
    private cartService: ShoppingCartService,
    private stripeCheckoutLoader: StripeCheckoutLoader) {
  }

  @Input() public productList: any = [];

  @Output() public passentry: EventEmitter<any> = new EventEmitter();

  ngOnInit() {
    this.userService.getUserProfile().subscribe(
      res => {
          this.userDetails = res;
      },
      err => {
        console.log(err);
      },
    );
    if (this.list.length === 0) {
      this.pushProductsList();
    }
    if (this.list.length !== 0) {
        let list;
        this.cartService.bSubject.subscribe(x => {list = x, console.log(x);
      });
      this.list = list;
    }
  }

  public ngAfterViewInit() {
    this.stripeCheckoutLoader.load();
    this.stripeCheckoutLoader.createHandler({
        key: 'pk_test_FMGLP7vKlKsjogGBkdxams5S00dXmbLOXT',
        token: (token) => {
            this.tokenId = token;
            this.paymentNumber = this.tokenId.created;
            console.log('Payment successful!', token);
            this.createPayment(this.paymentNumber);
        },
    }).then((handler: StripeCheckoutHandler) => {
      console.log(handler);
        this.stripeCheckoutHandler = handler;
    });
  }

  public onClickBuy() {
    this.stripeCheckoutHandler.open({
      name: 'Demo Pay',
      description: 'Order payment',
        amount: this.orderAmount * 100,
        currency: '$',
    });
  }

  public onClickCancel() {
      this.stripeCheckoutHandler.close();
  }

  createPayment(paymentNumber) {
    this.service.createPayment(this.orderId, paymentNumber).subscribe(res => {
      console.log(res);
      if (res === 201) {
        console.log(201);
      }
    },
    err => {
      console.log(err);
    });
  }

  onSave() {
    const orderList = {
      Email : this.userDetails.email,
      UserId : this.userDetails.id,
      UserName: this.userDetails.email,
      OrderAmount: this.orderAmount,
      Products : this.list
    };
    this.service.createNewOrder(orderList).subscribe(
      (res: any ) => {
        console.log(res);
        if (res !== 0) {
          this.orderId = res;
          console.log(201);
        }
      },
      err => {
        console.log(err);
      }
    );

    this.onClickBuy();
    this.onClose();
  }

  getProductQuantity(currentProduct) {
    let list;
    this.cartService.bSubject.subscribe(x => list = x);
    const product = list.find(prodId => prodId.prtintingEditionId === currentProduct.prtintingEditionId);

    return product.printinEditionQuantityForOrder;
  }

  setPlusProductQuantity(currentProduct) {
    this.list.find(prodId => prodId.prtintingEditionId === currentProduct.prtintingEditionId).printinEditionQuantityForOrder += 1;
    this.cartService.bSubject.next(this.list);

    this.updateLocalStorage(this.list);
  }

  setMinusProductQuantity(currentProduct) {
    this.list.find(prodId => prodId.prtintingEditionId === currentProduct.prtintingEditionId).printinEditionQuantityForOrder -= 1;
    this.cartService.bSubject.next(this.list);

    this.updateLocalStorage(this.list);
  }

  getTotalPrice() {
    let list;
    let totalPrice = 0;
    this.cartService.bSubject.subscribe(x => list = x);

    for (const i of list.length) {
      const quantity = list[i].printinEditionQuantityForOrder;
      const price = list[i].printingEditionPrice;
      totalPrice += (quantity * price);
    }

    this.orderAmount = totalPrice;

    return totalPrice;
  }

  getAmount(currentProduct) {
    let list;
    this.cartService.bSubject.subscribe(x => list = x);
    const product = list.find(prodId => prodId.prtintingEditionId === currentProduct.prtintingEditionId);
    const quantity = product.printinEditionQuantityForOrder;
    const amount = quantity * product.printingEditionPrice;

    return amount;
  }

  pushProductsList() {
    this.cartService.setSubjectList(this.productList);
    this.cartService.bSubject.subscribe(x => this.list = x);
  }

  updateLocalStorage(list) {
    localStorage.removeItem('productList');
    localStorage.setItem('productList', JSON.stringify(list));

    this.ngOnInit();
  }

  delete(currentProduct) {
    let list;
    this.cartService.bSubject.subscribe(x => list = x);
    const index = list.findIndex(product => product.prtintingEditionId === currentProduct.prtintingEditionId);

    list.splice(index, 1);

    this.cartService.bSubject.next(list);

    this.updateLocalStorage(list);
    this.ngOnInit();
  }

  onClose() {
    this.activeModal.close();
  }
}
