import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { PrintStoreService } from '../../services/print.store.service';
import { PrintStatus } from 'src/app/enums/print.status';
import { PrintType } from 'src/app/enums/print.type';

@Component({
  selector: 'app-store-item',
  templateUrl: './print.store.item.component.html',
  styleUrls: ['./print.store.item.component.css']
})

export class PrintStoreItemComponent implements OnInit {

  public printingEdition;
  public id;

  constructor(
    private route: ActivatedRoute, public formBuilder: FormBuilder, private service: PrintStoreService,
    public activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.id = params['PrtintingEditionId'];
    });

    this.service.getPrintingEditionsById(this.id).subscribe(res => {
      this.printingEdition = res;
      console.log(res);
    },
    err => {
      console.log(err);
    });
  }

  getStatus(statusEd) {
    if (statusEd === PrintStatus.Available) {
     return 'Available';
    }
    if (statusEd === PrintStatus.NotAvailable) {
     return 'Not available';
    }
    if (statusEd === PrintStatus.UnderTheOrder) {
     return 'Under the order';
    }
  }

  getColor(statusEd) {
    if (statusEd === PrintStatus.Available) {
      return 'green';
     }
    if (statusEd === PrintStatus.NotAvailable) {
    return 'red';
    }
    if (statusEd === PrintStatus.UnderTheOrder) {
    return 'orange';
    }
  }

 getType(typeEd) {
   let type;
   if (typeEd === PrintType.Magazine) {
     type = 'Magazine';
   }
   if (typeEd === PrintType.Book) {
     type = 'Book';
   }
   if (typeEd === PrintType.NewsPaper) {
     type = 'News paper';
   }
   return type;
 }
}
