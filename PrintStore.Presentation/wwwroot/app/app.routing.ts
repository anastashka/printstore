import { Routes, RouterModule } from '@angular/router';
import { UserComponent } from './user/user.component';
import { RegistrationComponent } from './user/registration/registration.component';



const routes: Routes = [
    {path:'', redirectTo: '/user/registration', pathMatch: 'full'},
    {    
        path: 'user', component: UserComponent, data: { title: 'User' },
        children: [
            { path: 'registration', component: RegistrationComponent, data: { title: 'Registration' }}
        ]
    }
];
export class AppRoutingModule{ }
export const routing = RouterModule.forRoot(routes);

export const routedComponents = [UserComponent, RegistrationComponent];