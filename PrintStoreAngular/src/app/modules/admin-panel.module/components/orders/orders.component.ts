import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AdminService } from '../../services/admin.service';
import { SortDirection } from 'src/app/enums/sort.direction';
import { PrintType } from 'src/app/enums/print.type';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styles: []
})

export class OrdersComponent implements OnInit {

  public ordersList;
  public sortDirection = SortDirection.Descending;
  public ordersModel = {
    Order: '',
    DateTime: '',
    UserName: '',
    Email: '',
    OrderAmount: '',
    OrderStatus: '',
    Products: ''
  };

  constructor(private service: AdminService, private router: Router) { }

  ngOnInit() {
    this.service.getAllUsersOrders().subscribe(
      res => {
          this.ordersList = res;
      },
      err => {
        console.log(err);
      },
    );
  }

  orderTimeSort() {
    localStorage.removeItem('sort');
    localStorage.setItem('sort', JSON.stringify('Order.DatePurchase'));
    if (this.sortDirection === SortDirection.None) {
      this.sortDirection = SortDirection.Ascending;
      this.ngOnInit();
      return;
    }
    if (this.sortDirection === SortDirection.Ascending) {
      this.service.orderSort(this.sortDirection).subscribe(res => {
        this.ordersList = res;
      });
      this.sortDirection = SortDirection.Descending;
    }
    if (this.sortDirection === SortDirection.Descending) {
      this.service.orderSort(this.sortDirection).subscribe(res => {
        this.ordersList = res;
      });
      this.sortDirection = SortDirection.None;
    }
  }

  filterByOrderStatusPaid() {
    const ordersStatusPayd = true;
    this.service.filterByOrderStatus(ordersStatusPayd).subscribe((res) => {
        this.ordersList = res;
    });
  }

  filterByOrderStatusNotPaid() {
    const ordersStatusNotPayd = false;
    this.service.filterByOrderStatus(ordersStatusNotPayd).subscribe((res) => {
      this.ordersList = res;
    });
  }

  orderNumberSort() {
    localStorage.removeItem('sort');
    localStorage.setItem('sort', JSON.stringify('Order.Id'));
    if (this.sortDirection === SortDirection.None) {
      this.sortDirection = SortDirection.Ascending;
      this.ngOnInit();
      return;
    }
    if (this.sortDirection === SortDirection.Ascending) {
      this.service.orderSort(this.sortDirection).subscribe(res => {
        this.ordersList = res;
      });
      this.sortDirection = SortDirection.Descending;
    }
    if (this.sortDirection === SortDirection.Descending) {
      this.service.orderSort(this.sortDirection).subscribe(res => {
        this.ordersList = res;
      });
      this.sortDirection = SortDirection.None;
    }
  }

  openEditModal(order) {

  }

  onLogout() {
    localStorage.removeItem('refreshToken');
    this.router.navigate(['/user/login']);
  }

  getType(typeEd) {
    let type;
    if (typeEd === PrintType.Magazine) {
      type = 'Magazine';
    }
    if (typeEd === PrintType.Book) {
      type = 'Book';
    }
    if (typeEd === PrintType.NewsPaper) {
      type = 'News paper';
     }
    return type;
   }
}
