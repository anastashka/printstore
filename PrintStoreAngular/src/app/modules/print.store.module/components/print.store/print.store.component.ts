import { Component, OnInit, Output, EventEmitter, ViewChild, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PrintStoreService } from '../../services/print.store.service';
import { ShoppingCartComponent } from '../shopping.cart/shopping.cart.component';
import { PrintStatus } from 'src/app/enums/print.status';
import { SortDirection } from 'src/app/enums/sort.direction';

@Component({
  selector: 'app-print.store',
  templateUrl: './print.store.component.html',
  styleUrls: ['./print.store.component.css'],
})

export class PrintStoreComponent implements OnInit {

  public printingEditions;
  public authorSearchString: string;
  public titleSearch: string;
  public descriptionSearch: string;
  public printStatus = PrintStatus;
  public sortDirection = SortDirection.Ascending;
  public productList: any [] ;

  constructor(public service: PrintStoreService, public router: Router, public formBuilder: FormBuilder, public modalService: NgbModal) { }

  ngOnInit() {
  }

  onLogout() {
    localStorage.removeItem('refreshToken');
    this.router.navigate(['/store/all']);
  }

  checkout() {
    this.productList = this.service.getLocalProductList();
    const modalRef = this.modalService.open(ShoppingCartComponent);
    modalRef.componentInstance.productList = this.productList;

    modalRef.result.then((result) => {
      if (result) {
        console.log(result);
      }
    });
  }
}
