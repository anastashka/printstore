import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-edit.author',
  templateUrl: './edit.author.component.html',
  styleUrls: []
})

export class EditAuthorComponent implements OnInit {

  @Input() public author;
  @Output() public passEntry: EventEmitter<any> = new EventEmitter();

  constructor(
    public activeModal: NgbActiveModal) { }

  ngOnInit() {
    console.log(this.author);
  }

  passBack() {
    this.passEntry.emit(this.author);
    this.activeModal.close(this.author);
  }

  onClose() {
    this.activeModal.close();
  }
}
