import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { UserService } from 'src/app/modules/user.module/services/user.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styles: []
})

export class EditUserComponent implements OnInit {

  public userDetails;
  public formPasswords = this.formBuilder.group({
    Id: '',
    NewPassword: ['', [ Validators.minLength(6)]],
    ConfirmNewPassword: ['', [ Validators.minLength(6)]],
    OldPassword: ''
  }, {validator: this.comparePasswords});

  public formModel = {
    Id: '',
    Email: '',
    FirstName: '',
    LastName: ''
  };

  constructor(private formBuilder: FormBuilder, private service: UserService, private toastr: ToastrService, private router: Router) {
    this.service.getUserProfile().subscribe(
      res => {
          this.userDetails = res;
      },
      err => {
        console.log(err);
      },
    );
  }

  ngOnInit() {

  }

  comparePasswords(formGroup: FormGroup) {
    const confirmPaswCtrl = formGroup.get('ConfirmNewPassword');
    if (confirmPaswCtrl.errors == null || 'passwordMismatch' in confirmPaswCtrl.errors) {
      confirmPaswCtrl.setErrors(null);
      if (formGroup.get('NewPassword').value !== confirmPaswCtrl.value) {
        confirmPaswCtrl.setErrors({passwordMismatch: true});
      }
    }
  }

  onPasswordSave() {
    this.formPasswords.value.Id = JSON.parse(window.atob(localStorage.getItem('refreshToken').split('.')[1])).UserID;

    this.service.changePassword(this.formPasswords.value).subscribe(
      (res: any ) => {
        console.log(res.succeeded);
        if (res.succeeded) {
          this.toastr.success('Password was changed successful');
        }
        res.errors.forEach(element => {
          this.toastr.error(element.code, 'Error');
        });
      },
      err => {
        console.log(err);
    });
  }

  onLogout() {
    localStorage.removeItem('refreshToken');
    this.router.navigate(['/store/all']);
  }

  onSave() {
    if (this.formModel.Email === '') {
      this.formModel.Email = this.userDetails.email;
    }
    if (this.formModel.FirstName === '') {
      this.formModel.FirstName = this.userDetails.firstName;
    }
    if (this.formModel.LastName === '') {
      this.formModel.LastName = this.userDetails.lastName;
    }

    this.formModel.Id = JSON.parse(window.atob(localStorage.getItem('refreshToken').split('.')[1])).UserID;

    this.service.edit(this.formModel).subscribe(
      (res: any ) => {
        if (res === 201) {
          console.log(res);
            this.toastr.success('Changes was save successful');
        }
        if (res !== 201) {
          this.toastr.error('Error');
        }
      },
      err => {
        console.log(err);
      }
    );
  }
}


