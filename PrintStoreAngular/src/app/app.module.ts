import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BsModalService, ModalModule } from 'ngx-bootstrap/modal';
import { AutoCompleteModule} from 'primeng/autocomplete';
import { MatAutocompleteModule, MatInputModule } from '@angular/material';
import { PrintStoreService } from './modules/print.store.module/services/print.store.service';
import { SpinnerModule} from 'primeng/spinner';
import { StripeCheckoutModule} from 'ng-stripe-checkout';
import { Module as StripeModule } from 'stripe-angular';
import { EditAuthorComponent } from './modules/admin-panel.module/components/authors/edit.author/edit.author.component';
import { AppNavComponent } from './app.nav/app.nav.component';
import { AdminService } from './modules/admin-panel.module/services/admin.service';
import { ShoppingCartService } from './modules/print.store.module/services/shopping.cart.service';
import { ApiInterceptor } from './modules/interceptor/api.interceptor';
import { UserService } from './modules/user.module/services/user.service';
import { AuthInterceptor } from './modules/interceptor/auth/auth.interceptor';
import { CreateAuthorComponent } from './modules/admin-panel.module/components/authors/create.author/create.author.component';
import { UserCreateComponent } from './modules/admin-panel.module/components/users/user.create/user.create.component';
import { UserEditComponent } from './modules/admin-panel.module/components/users/user.edit/user.edit.component';
import { AddPrintingComponent } from './modules/admin-panel.module/components/printing-editions/add.printing/add.printing.component';
import { EditPrintingComponent } from './modules/admin-panel.module/components/printing-editions/edit.printing/edit.printing.component';
import { ShoppingCartComponent } from './modules/print.store.module/components/shopping.cart/shopping.cart.component';

@NgModule({
  declarations: [
    AppComponent,
    AppNavComponent,
    EditAuthorComponent,
    CreateAuthorComponent,
    UserCreateComponent,
    UserEditComponent,
    AddPrintingComponent,
    EditPrintingComponent,
    ShoppingCartComponent
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  entryComponents: [
    EditAuthorComponent,
    CreateAuthorComponent,
    UserCreateComponent,
    UserEditComponent,
    AddPrintingComponent,
    EditPrintingComponent,
    ShoppingCartComponent
  ],
  imports: [
    BrowserModule,
    MatAutocompleteModule,
    MatInputModule,
    AppRoutingModule,
    ReactiveFormsModule,
    NgbModule,
    HttpClientModule,
    AutoCompleteModule,
    StripeCheckoutModule,
    BrowserAnimationsModule, // required animations module
    StripeModule.forRoot(),
    ToastrModule.forRoot({
      progressBar: true
    }),
    FormsModule,
    SpinnerModule,
    ModalModule.forRoot()
  ],
  providers: [ UserService,
     PrintStoreService,
     AdminService,
     BsModalService,
     ShoppingCartService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ApiInterceptor,
      multi: true },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
  }],
  bootstrap: [AppComponent]
})

export class AppModule { }
