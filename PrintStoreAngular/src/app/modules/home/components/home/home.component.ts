import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../../../user.module/services/user.service';
import { SortDirection } from 'src/app/enums/sort.direction';
import { PrintType } from 'src/app/enums/print.type';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: []
})

export class HomeComponent implements OnInit {

  public userDetails;
  public ordersList;
  public sortDirection = SortDirection.Descending;
  public ordersModel = {
    Order: '',
    DateTime: '',
    UserName: '',
    Email: '',
    OrderAmount: '',
    OrderStatus: '',
    Products: ''
  };

  constructor(private router: Router, private service: UserService) {}

  ngOnInit() {
    this.service.getUserProfile().subscribe(
      res => {
          this.userDetails = res;
      },
      err => {
        console.log(err);
      },
    );

    this.service.getUserOrders().subscribe(
      res => {
          this.ordersList = res;
          console.log(this.ordersList);
      },
      err => {
        console.log(err);
      },
    );
  }

  getType(typeEd) {
    let type;
    if (typeEd === PrintType.Magazine) {
      type = 'Magazine';
    }
    if (typeEd === PrintType.Book) {
      type = 'Book';
    }

    if (typeEd === PrintType.NewsPaper) {
      type = 'News paper';
    }
    return type;
   }

  onLogout() {
    localStorage.removeItem('refreshToken');
    this.router.navigate(['/user/login']);
  }
}
