import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-user.edit',
  templateUrl: './user.edit.component.html',
  styles: []
})

export class UserEditComponent implements OnInit {

  @Input() public user = {
    Id: '',
    Email: '',
    FirstName: '',
    LastName: ''
  };

  @Output() public passEntry: EventEmitter<any> = new EventEmitter();

  constructor(
    public activeModal: NgbActiveModal) { }

  ngOnInit() {
    console.log(this.user);
  }

  saveUser() {
    this.passEntry.emit(this.user);
    this.activeModal.close(this.user);
  }

  onClose() {
    this.activeModal.close();
  }
}
