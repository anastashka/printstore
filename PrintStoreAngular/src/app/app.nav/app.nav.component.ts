import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PrintStoreService } from '../modules/print.store.module/services/print.store.service';
import { ShoppingCartComponent } from '../modules/print.store.module/components/shopping.cart/shopping.cart.component';

@Component({
  selector: 'app-nav-menu',
  templateUrl: './app.nav.component.html',
  styleUrls: ['./app.nav.component.css']
})

export class AppNavComponent implements OnInit {

  public productList: any;

  constructor(private router: Router, public service: PrintStoreService, public modalService: NgbModal) { }

  ngOnInit() {
  }

  isLogged() {
    if (localStorage.getItem('refreshToken') != null){
      return true;
    }
    return false;
  }

  onLogout(){
    localStorage.removeItem('refreshToken');
    this.router.navigate(['/store/all']);
  }

  checkout() {
    this.productList = this.service.getLocalProductList();
    const modalRef = this.modalService.open(ShoppingCartComponent);
    modalRef.componentInstance.productList = this.productList;
    modalRef.result.then((result) => {
      if (result) {
        console.log(result);
      }
    });
  }
}
