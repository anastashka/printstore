import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/modules/user.module/services/user.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';


@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styles: []
})

export class RegistrationComponent implements OnInit {

  constructor(public service: UserService, private toastr: ToastrService, private router: Router) { }

  ngOnInit() {
    this.service.formModel.reset();
  }

  onSubmit() {
    if (this.service.formModel.invalid) {
      this.toastr.error('Invalid form');
    }

    this.service.register().subscribe(
      (res: any ) => {
        if (res === 200) {
          this.toastr.success('New user created', 'Registration successful');
          this.router.navigateByUrl('user/confirm');
        }
        if (res !== 200) {
          res.errors.forEach(element => {
          console.log(element);
          this.toastr.error(element.code, 'registration failed');
          });
        }
      },
      err => {
        console.log(err);
      }
    );
  }
}
