﻿using Microsoft.AspNetCore.Hosting;

[assembly: HostingStartup(typeof(PrintStore.Presentation.Areas.Identity.IdentityHostingStartup))]
namespace PrintStore.Presentation.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) =>
            {
            });
        }
    }
}