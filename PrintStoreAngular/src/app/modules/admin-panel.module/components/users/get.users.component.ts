import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UserEditComponent } from './user.edit/user.edit.component';
import { UserCreateComponent } from './user.create/user.create.component';
import { AdminService } from '../../services/admin.service';
import { SortDirection } from 'src/app/enums/sort.direction';

@Component({
  selector: 'app-users',
  templateUrl: './get.users.component.html',
  styles: []
})
export class GetUsersComponent implements OnInit {

  public usersList;
  public sortDirection = SortDirection.Ascending;
  public sortFirstName = 'FirstName';
  public sortLastName = 'LastName';
  public userModel = {
    Id: '',
    Email: '',
    FirstName: '',
    LastName: ''
  };

  constructor(private service: AdminService, private router: Router, public modalService: NgbModal) { }

  ngOnInit() {
    this.service.getAllUsers().subscribe(
      res => {
          this.usersList = res;
      },
      err => {
        console.log(err);
      },
    );
  }

  openEditModal(entity) {
    const user = {
      FirstName: entity.firstName,
      LastName: entity.lastName,
      Id: entity.id,
      Email: entity.email
    };

    const modalRef = this.modalService.open(UserEditComponent);
    modalRef.componentInstance.user = user;
    modalRef.result.then((result) => {
      if (result) {
        this.edit(result);
        console.log(result);
      }
    });
  }

  openCreateModal() {
    const modalRef = this.modalService.open(UserCreateComponent);

    modalRef.result.then((result) => {
      if (result) {
        this.create(result);
        console.log(result);
      }
    });
  }

  create(form) {
    this.service.createUser(form).subscribe(
      () => {
          this.ngOnInit();
      },
      err => {
        console.log(err);
      },
    );
  }

  edit(user) {
    this.service.editUser(user).subscribe(
      () => {
          this.ngOnInit();
      },
      err => {
        console.log(err);
      },
    );
  }

  delete(user) {
    const id = user.id;
    this.service.deleteUser(id).subscribe(
      () => {
        this.ngOnInit();
      },
      err => {
        console.log(err);
      }
    );
  }

  sortByName(sortColumn) {
    if (this.sortDirection === SortDirection.None) {
      this.sortDirection = SortDirection.Ascending;
      this.ngOnInit();
      return;
    }
    if (this.sortDirection === SortDirection.Ascending) {
      this.sorting(this.sortDirection, sortColumn);
      this.sortDirection = SortDirection.Descending;
    }
    if (this.sortDirection === SortDirection.Descending) {
      this.sorting(this.sortDirection, sortColumn);
      this.sortDirection = SortDirection.None;
    }
  }

  sorting(sortDirection, sortColumn) {

    if (sortColumn === this.sortFirstName) {
      this.service.getSortedUsersByFirstName(sortDirection).subscribe(res => {
        this.usersList = res;
    },
      err => {
        console.log(err);
      });
    }

    this.service.getSortedUsersByLastName(sortDirection).subscribe(res => {
      this.usersList = res;
      },
      err => {
        console.log(err);
    });
  }

  onLogout() {
    localStorage.removeItem('refreshToken');
    this.router.navigate(['/user/login']);
  }
}
