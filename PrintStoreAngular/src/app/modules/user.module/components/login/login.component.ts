import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { UserService } from 'src/app/modules/user.module/services/user.service';
import { Router, NavigationExtras } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styles: []
})

export class LoginComponent implements OnInit {

  public tryLoginFailed: boolean = false;
  public formModel = this.formBuilder.group({
    email: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.required, Validators.minLength(4)]]
  })

  
  constructor(private service: UserService, private router: Router, private toastr: ToastrService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    if (localStorage.getItem('refreshToken') != null){
      this.router.navigateByUrl('/home');
    }      
  }

  get formControls() { return this.formModel.controls; }

  isLoginFailed(){
    return this.tryLoginFailed;
  }

  openPasswordRecovery(){
    let navigationExtras: NavigationExtras = {
      queryParams: {
        'email': this.formModel.get('email').value
      }
    };

    this.router.navigate(["/user/recovery"], navigationExtras);    
  }

  onSubmit() {
    this.formModel.value;
    
    this.service.login(this.formModel.value).subscribe(
      (res: any) => {
        localStorage.setItem('refreshToken', res.refreshToken);
        this.router.navigateByUrl('/store/all');
      },
      err => {
        if (err.status == 400){
          this.tryLoginFailed = true;
          this.toastr.error('Incorrect username or password.', 'Authentication failed.');        
        }          
      }
    );
  }
}