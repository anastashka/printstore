import { NgModule } from '@angular/core';
import { Routes, Router, RouterModule } from '@angular/router';
import { ForbiddenComponent } from './components/forbidden/forbidden.component';


const routes: Routes = [
  {path: '', component: ForbiddenComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]

})
export class ForbiddenRoutingModule { }
