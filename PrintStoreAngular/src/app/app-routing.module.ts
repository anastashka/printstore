import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './modules/interceptor/auth/auth.guard';

const routes: Routes = [
  {path: '', redirectTo: 'store/all', pathMatch: 'full'},
  {path: 'store', loadChildren: './modules/print.store.module/print.store.module#PrintStoreModule'},
  {path: 'adminpanel', loadChildren: './modules/admin-panel.module/admin.panel.module#AdminPanelModule'},
  {path: 'home', canActivate: [AuthGuard], loadChildren: './modules/home/home.module#HomeModule'},
  {path: 'user', loadChildren: './modules/user.module/user.module.module#UserModuleModule'},
  {path: 'forbidden', loadChildren: './modules/forbidden/forbidden.module#ForbiddenModule'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
