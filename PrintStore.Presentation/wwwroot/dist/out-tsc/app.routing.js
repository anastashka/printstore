import { RouterModule } from '@angular/router';
import { UserComponent } from './user/user.component';
import { RegistrationComponent } from './user/registration/registration.component';
var routes = [
    { path: '', redirectTo: '/user/registration', pathMatch: 'full' },
    {
        path: 'user', component: UserComponent, data: { title: 'User' },
        children: [
            { path: 'registration', component: RegistrationComponent, data: { title: 'Registration' } }
        ]
    }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    return AppRoutingModule;
}());
export { AppRoutingModule };
export var routing = RouterModule.forRoot(routes);
export var routedComponents = [UserComponent, RegistrationComponent];
//# sourceMappingURL=app.routing.js.map