import { SortDirection } from './../../../enums/sort.direction';
import { PrintStatus } from 'src/app/enums/print.status';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { PrintType } from 'src/app/enums/print.type';



@Injectable({
    providedIn: 'root'
  })

export class PrintStoreService {

  public printingEditions;
  public dafaultElementId = 0;
  public storeItemsCount;
  public productList: any [] = [];
  public amountOfElementsOnPage = 4;

  constructor(private http: HttpClient) {
    this.printingEditions = this.getNextPageOfPrintingEditions(this.dafaultElementId, 0, null);
    this.storeItemsCount = this.getCountOfEditionsInStore();
  }

  getLocalProductList() {
    const productList: any [] = JSON.parse(localStorage.getItem('productList'));

    return productList;
  }

  addToBasket(printingEdition) {
    if (localStorage.getItem('productList')) {
      this.productList = JSON.parse(localStorage.getItem('productList'));
      this.productList.push(JSON.stringify(printingEdition));
      localStorage.setItem('productList', JSON.stringify(this.productList));
    }
    if (localStorage.getItem('productList') == null) {
      this.productList.push(JSON.stringify(printingEdition));
      localStorage.setItem('productList', JSON.stringify(this.productList));
    }

    return;
  }

  createPayment(orderId: any, paymentNumber) {
    const body = {
      OrderId: orderId,
      IsPayed: true,
      PaymentNumber: paymentNumber
    };

    return this.http.post('/printStore/CreatePayment', body);
  }

  sortByPriceRange(from: number, to: number, firstElement) {
    const currency = localStorage.getItem('currentCurrencyName');
    const options = { params: new HttpParams().set('sortPriceFrom', from.toString())
        .set('sortPriceTo', to.toString())
        .set('currentCurrencyName', currency) };

    return this.http.get('/printStore/GetSortedPriceInRange', options);
  }

  createNewOrder(orderLict: any) {
    return this.http.post('/printStore/CreateNewOrder', orderLict);
  }

  getPrintingEditionsById(id) {
    const currency = localStorage.getItem('currentCurrencyName');
    const options = { params: new HttpParams().set('id', id.toString())
      .set('currentCurrencyName', currency) };

    return this.http.get('/printStore/GetPrintingEdition', options);
  }

  isLogged() {
    if (localStorage.getItem('refreshToken') != null) {
      return true;
    }
    return false;
  }

  searchAnyProduct(titleSearchString: string, item) {
    const lastElement = item + this.amountOfElementsOnPage;
    const currency = localStorage.getItem('currentCurrencyName');
    let columnFilter = JSON.parse(localStorage.getItem('filter'));
    let peType = localStorage.getItem('peType');
    let peStatus = localStorage.getItem('peStatus');

    if (columnFilter === null) {
      columnFilter = 'none';
    }
    if (titleSearchString === null) {
      titleSearchString = 'none';
    }
    if (peType === null) {
        peType = PrintType.None.toString();
    }
    if (peStatus === null) {
      peStatus = PrintStatus.None.toString();
    }

    const options = { params: new HttpParams().set('firstElement', item.toString())
      .set('lastElement', lastElement.toString())
      .set('currentCurrencyName', currency)
      .set('order', SortDirection.None.toString())
      .set('column', columnFilter)
      .set('filter', titleSearchString)
      .set('type', peType)
      .set('status', peStatus)
    };
    return this.http.get('/printStore/FilterByAll', options);
  }

  sortAuthorByFirstName(sortDirection, firstElement) {
    const currency = localStorage.getItem('currentCurrencyName');
    const options = { params: new HttpParams().set('value', sortDirection.toString())
      .set('currentCurrencyName', currency) };
    return this.http.get('/printStore/SortByAuthorsFirstName', options);
  }

  getProductsList(value: any) {
      return this.http.get('/printStore/GetAllAuthorsInPrintingEditions');
  }

  searchDescription(descriptionSearchString: string, firstElement) {
    const currency = localStorage.getItem('currentCurrencyName');
    const options = { params: new HttpParams().set('filter', descriptionSearchString.toString())
      .set('currentCurrencyName', currency) };
    return this.http.get('/printStore/FilterByPrintingEditionDescription', options);
  }

  searchTitle(titleSearchString: string) {
    const currency = localStorage.getItem('currentCurrencyName');
    const options = { params: new HttpParams().set('filter', titleSearchString.toString())
      .set('currentCurrencyName', currency) };
    return this.http.get('/printStore/FilterByPrintingEditionTitle', options);
  }

  searchAuthor(authorSearchString: string) {
    const currency = localStorage.getItem('currentCurrencyName');
    const options = { params: new HttpParams().set('filter', authorSearchString.toString())
      .set('currentCurrencyName', currency) };

    return this.http.get('/printStore/FilterByAuthors', options);
  }

  getAuthorsByName(query: any) {
    return this.http.get(`/printStore/GetFilteredAuthors?filter=${query}`);
  }

  getCurrencies() {
    return this.http.get('/printStore/GetCurrencies');
  }

  sortByPrice(direction, firstElement) {
    const currency = localStorage.getItem('currentCurrencyName');
    const options = { params: new HttpParams().set('value', direction.toString())
      .set('currentCurrencyName', currency) };

    return this.http.get('/printStore/SortByPrintingEditionPrice', options);
  }

  getAllAuthors() {
    return this.http.get('/admin/GetAllAuthors');
  }

  getNextPageOfPrintingEditions(item, sortOrder, filter) {
    const lastElement = item + this.amountOfElementsOnPage;
    const currency = localStorage.getItem('currentCurrencyName');
    let columnFilter = JSON.parse(localStorage.getItem('filter'));
    let peType = localStorage.getItem('peType');
    let peStatus = localStorage.getItem('peStatus');

    if (columnFilter === null) {
      columnFilter = 'none';
    }
    if (filter === null) {
      filter = 'none';
    }
    if (peType === null) {
        peType = PrintType.None.toString();
    }
    if (peStatus === null) {
      peStatus = PrintStatus.None.toString();
    }

    const options = { params: new HttpParams().set('firstElement', item.toString())
      .set('lastElement', lastElement.toString())
      .set('currentCurrencyName', currency)
      .set('order', sortOrder)
      .set('column', columnFilter)
      .set('filter', filter)
      .set('type', peType)
      .set('status', peStatus)
    };

    return this.http.get('/printStore/GetAuthorsInPrintingEditionsPage', options);
  }

  getPreviousPageOfPrintingEditions(item, sortOrder, filter) {
    const previousElement = item - this.amountOfElementsOnPage;
    const currency = localStorage.getItem('currentCurrencyName');
    let columnFilter = JSON.parse(localStorage.getItem('filter'));
    let peType = localStorage.getItem('peType');
    let peStatus = localStorage.getItem('peStatus');

    if (columnFilter === null) {
      columnFilter = 'none';
    }
    if (filter === null) {
      filter = 'none';
    }
    if (peType === null) {
        peType = PrintType.None.toString();
    }
    if (peStatus === null) {
      peStatus = PrintStatus.None.toString();
    }

    const options = { params: new HttpParams().set('firstElement', previousElement.toString())
      .set('lastElement', item.toString())
      .set('currentCurrencyName', currency)
      .set('order', sortOrder)
      .set('column', columnFilter)
      .set('filter', filter)
      .set('type', peType)
      .set('status', peStatus)
    };
    return this.http.get('/printStore/GetAuthorsInPrintingEditionsPage', options);
  }

  getAllPrintingEditions() {
    return this.http.get('/printStore/GetAllAuthorsInPrintingEditions');
  }

  getCountOfEditionsInStore(): any {
    return this.http.get('/printStore/GetCountOfEditionsInStore');
  }
}
