import { NewPasswordGenerateComponent } from './components/new.password.generate/new.password.generate.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserComponent } from './components/user/user.component';
import { UserModuleRoutingModule } from './user.module.routing.module';
import { RegistrationComponent } from './components/registration/registration.component';
import { LoginComponent } from './components/login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConfirmEmailComponent } from './components/confirm.email/confirm.email.component';
import { EditUserComponent } from './components/edit.user.profile/edit.component';
import { PasswordRecoveryComponent } from './components/password.recovery/password.recovery.component';

@NgModule({
  declarations: [UserComponent,
    RegistrationComponent,
    LoginComponent,
    EditUserComponent,
    ConfirmEmailComponent,
    PasswordRecoveryComponent,
    NewPasswordGenerateComponent
  ],
  imports: [
    CommonModule,
    UserModuleRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class UserModuleModule { }
