import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EditAuthorComponent } from './edit.author/edit.author.component';
import { CreateAuthorComponent } from './create.author/create.author.component';
import { AdminService } from '../../services/admin.service';
import { SortDirection } from 'src/app/enums/sort.direction';

@Component({
  selector: 'app-authors',
  templateUrl: './authors.component.html',
  styles: []
})

export class AuthorsComponent implements OnInit {

  public authorsList;
  public authorSearchString;
  public sortFirstName = 'FirstName';
  public sortLastName = 'LastName';
  public sortDirection = SortDirection.Ascending;
  public authorModel = {
    FirstName: '',
    LastName: '',
    FullName: '',
    Id: ''
  };

  constructor(private service: AdminService, private router: Router,  public modalService: NgbModal) { }

  search() {
      this.service.searchAuthorForAuthorTable(this.authorSearchString).subscribe(res => {
        this.authorsList = res;
    },
    err => {
      console.log(err);
    });
  }

  ngOnInit() {
    this.service.getAllAuthors().subscribe(
      res => {
          this.authorsList = res;
      },
      err => {
        console.log(err);
      },
    );
  }

  openCreateModal() {
    const modalRef = this.modalService.open(CreateAuthorComponent);

    modalRef.result.then((result) => {
      if (result) {
        this.create(result);
        console.log(result);
      }
    });
  }

  openEditModal(entity) {
    const author = {
      FirstName: entity.firstName,
      LastName: entity.lastName,
      Id: entity.id
    };

    const modalRef = this.modalService.open(EditAuthorComponent);

    modalRef.componentInstance.author = author;
    modalRef.result.then((result) => {
      if (result) {
        this.edit(result);
        console.log(result);
      }
    });
  }

  sortByName(sortColumn) {
    if (this.sortDirection === SortDirection.None) {
      this.sortDirection = SortDirection.Ascending;
      this.ngOnInit();
      return;
    }
    if (this.sortDirection === SortDirection.Ascending) {
      this.sorting(this.sortDirection, sortColumn);
      this.sortDirection = SortDirection.Descending;
    }
    if (this.sortDirection === SortDirection.Descending) {
      this.sorting(this.sortDirection, sortColumn);
      this.sortDirection = SortDirection.None;
    }
  }

  sorting(sortDirection, sortColumn) {
    if (sortColumn === this.sortFirstName) {
      this.service.GetSortedAuthorsByName(sortDirection, sortColumn).subscribe(res => {
        this.authorsList = res;
    },
      err => {
        console.log(err);
      });
    }

    this.service.GetSortedAuthorsByName(sortDirection, sortColumn).subscribe(res => {
      this.authorsList = res;
      },
      err => {
        console.log(err);
    });
  }

  create(form) {

    this.service.createAuthor(form).subscribe(
      () => {
          this.ngOnInit();
      },
      err => {
        console.log(err);
      },
    );
  }

  edit(formData) {
    this.service.editAuthor(formData).subscribe(
      () => {
          this.ngOnInit();
      },
      err => {
        console.log(err);
      },
    );
  }

  delete(form) {
    const id = form.id;
    this.service.deleteAuthor(id).subscribe(
      res => {
          this.ngOnInit();
      },
      err => {
        console.log(err);
      },
    );
  }

  onLogout() {
    localStorage.removeItem('refreshToken');
    this.router.navigate(['/user/login']);
  }
}
