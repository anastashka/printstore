import { Injectable } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class UserService {

  constructor(private formBuilder: FormBuilder, private http: HttpClient) { }

  formModel = this.formBuilder.group({
    Email: ['', [Validators.required, Validators.email]],
    Passwords: this.formBuilder.group({
      Password: ['', [Validators.required, Validators.minLength(6)]],
      PasswordConfirm: ['', Validators.required]
    }, {validator: this.comparePasswords})

  });

  confirmPassword() {

  }

  comparePasswords(formGroup: FormGroup) {
    const confirmPaswCtrl = formGroup.get('PasswordConfirm');
    confirmPaswCtrl.setErrors(null);
    if (confirmPaswCtrl.errors == null || 'passwordMismatch' in confirmPaswCtrl.errors) {
      if (formGroup.get('Password').value !== confirmPaswCtrl.value) {
        confirmPaswCtrl.setErrors({passwordMismatch: true});
      }
    }
  }

  register() {
    const body = {
      Email: this.formModel.value.Email,
      Password: this.formModel.value.Passwords.Password,
      PasswordConfirm: this.formModel.value.Passwords.PasswordConfirm
    };

    return this.http.post('/account/register', body);
  }

  login(formData) {
    return this.http.post('/account/login', formData);
  }

  edit(formData) {
    return this.http.post('/users/edit', formData);
  }

  changePassword(formData) {
    return this.http.post('/users/ChangePassword', formData);
  }

  getUserProfile() {
    return this.http.get('/users/GetUserProfile');
  }

  confirmEmail() {
    const body = {
      Email: this.formModel.value.Email,
      Password: this.formModel.value.Passwords.Password,
      PasswordConfirm: this.formModel.value.Passwords.PasswordConfirm
    };

    return this.http.post('/account/ConfirmEmailAsync', body);
  }

  roleMatch(allowedRoles): boolean {
    let isMatch = false;
    const payLoad = JSON.parse(window.atob(localStorage.getItem('refreshToken').split('.')[1]));
    const userRole = payLoad.role;
    allowedRoles.forEach(element => {
      if (userRole === element) {
        isMatch = true;
        return false;
      }
    });

    return isMatch;
  }

  getUserOrders() {
    const id = JSON.parse(window.atob(localStorage.getItem('refreshToken').split('.')[1])).UserID;

    return this.http.get(`/users/GetUserOrders?id=${id}`);
  }

  forgotPassword(email: any) {
    const body = {
      Email: email
    };
    return this.http.post('/account/ForgotPassword', body);
  }
}
