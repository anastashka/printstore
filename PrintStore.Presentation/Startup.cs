﻿using AutoMapper;
using Microsoft.OpenApi.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using PrintStore.BusinessLogic.Helpers;
using PrintStore.BusinessLogic.Mapping;
using PrintStore.DataAccess;
using PrintStore.DataAccess.Entities.Base;
using PrintStore.Presentation.Middlewares;
using Stripe;
using System;
using System.IO;
using Microsoft.AspNetCore.Routing;

namespace PrintStore.Presentation
{
    public class Startup
    {
        private readonly IConfiguration config;

        public Startup(IConfiguration config)
        {
            this.config = config;
        }

        public void ConfigureServices(IServiceCollection services)
        {            
            services.AddDbContext<StoreDBContext>(options =>
                    options.UseSqlServer(config.GetConnectionString("DefaultConnection")));

            services.InjectDataBase(config);

            MapperConfiguration mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });

            IMapper mapper = mappingConfig.CreateMapper();

            services.AddSingleton(mapper);
            services.AddMvc()
                .AddSessionStateTempDataProvider();
            services.AddSession();
            services.AddCors();
            services
                .AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(cfg =>
                {
                    cfg.RequireHttpsMetadata = false;
                    cfg.SaveToken = false;
                    cfg.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = false,
                        ValidateAudience = false,
                        ClockSkew = TimeSpan.Zero,
                        IssuerSigningKey = new JwtHelper().GetSymmetricSecurityKey(),
                        ValidateIssuerSigningKey = true
                    };
                });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Print store", Version = "v1" });
            });
        }

        [Obsolete]
        public async void Configure(IApplicationBuilder applicationBuilder,
            IServiceProvider serviceProvider,
            IHostingEnvironment env,
            ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                applicationBuilder.UseDeveloperExceptionPage();
            }
            if (!env.IsDevelopment())
            {
                applicationBuilder.UseHsts();
            }

            StripeConfiguration.SetApiKey(config.GetSection("Stripe")["SecretKey"]);

            loggerFactory.AddConsole(config.GetSection("Logging"));
            loggerFactory.AddDebug();
            loggerFactory.AddLog4Net(config.GetValue<string>("Log4NetConfigFile:Name"));

            applicationBuilder.UseSwagger();
            applicationBuilder.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Print store V1");
            });
            applicationBuilder.UseHttpsRedirection();
            applicationBuilder.UseDefaultFiles();
            applicationBuilder.UseMiddleware<ExceptionHandlerMiddleware>();
            applicationBuilder.UseAuthentication();
            applicationBuilder.UseHttpsRedirection();
            applicationBuilder.UseStaticFiles();
            applicationBuilder.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(Path.Combine(env.ContentRootPath, "node_modules")),
                RequestPath = "/node_modules"
            });
            applicationBuilder.UseCors(builder =>
                builder.AllowAnyHeader()
                .AllowAnyOrigin()
                .AllowCredentials()
                .AllowAnyMethod());
            applicationBuilder.UseMvc();
            applicationBuilder.UseSession();
            applicationBuilder.UseMvc(routes =>
            {
                routes.MapRoute(
                name: "default",
                template: "{controller=Home}/{action=Index}/{id?}");
            });
            await applicationBuilder.UseDataBase(serviceProvider);
        }
    }
}
