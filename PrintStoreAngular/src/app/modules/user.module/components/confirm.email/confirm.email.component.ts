import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/modules/user.module/services/user.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-confirm.email',
  templateUrl: './confirm.email.component.html',
  styles: []
})

export class ConfirmEmailComponent implements OnInit {

  constructor(private service: UserService, private router: Router, private toastr: ToastrService) { }

  ngOnInit() {
  }

  onConfirm() {
    this.service.confirmEmail().subscribe((res: any) => {
      this.service.formModel.reset();
      if (res === 200) {
        this.toastr.error('Email sent'),
          this.router.navigateByUrl('/user/login');
      }
      if (res !== 200) {
        this.toastr.error('Cant send mail');
        this.router.navigateByUrl('/user/login');
      }
    }, err => {
      console.log(err);
    });
  }
}
